module.exports = class CompressImgWebpackPlugin {
  apply(compiler) {
    // compiler.hooks.emit.tapAsync(
    //   'CompressImgWebpackPlugin',
    //   (compilation, callback) => {
    //     console.log('This is an example plugin!')
    //     console.log(
    //       'Here’s the `compilation` object which represents a single build of assets:',
    //       compilation
    //     )
    //     // compilation.addModule()
    //     callback()
    //   }
    // )

    compiler.hooks.compilation.tap(
      'CompressImgWebpackPlugin',
      (compilation) => {
        compilation.hooks.optimize.tap('CompressImgWebpackPlugin', () => {
          console.log('optimizing...')
        })
      }
    )

    compiler.hooks.done.tap('CompressImgWebpackPlugin', (stats) => {
      console.log('compress img')
    })

    compiler.hooks.emit.tapAsync(
      'CompressImgWebpackPlugin',
      (compilation, callback) => {
        let filelist = 'In this build:\n\n'
        for (let filename in compilation.assets) {
          filelist += `- ${filename}\n`
        }

        compilation.assets['filelist.md'] = {
          source: () => filelist,
          size: () => filelist.length,
        }

        callback()
      }
    )
  }
}
