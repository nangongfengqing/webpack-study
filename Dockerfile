FROM node
RUN mkdir -p /docker-dev
WORKDIR /docker-dev
COPY . .
RUN npm config set registry https://registry.npmmirror.com/
RUN npm i
CMD npm run dev
EXPOSE 8080