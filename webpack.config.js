const path = require('path')
// const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
// const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
//   .BundleAnalyzerPlugin
// const WorkboxPlugin = require('workbox-webpack-plugin')
// const ImageMinimizerPlugin = require('image-minimizer-webpack-plugin')
// const TinyimgPlugin = require('tinyimg-webpack-plugin')
// const CompressImgWebpackPlugin = require('./plugin.js')
const SpeedMeasurePlugin = require('speed-measure-webpack-plugin')
const ESLintPlugin = require('eslint-webpack-plugin')
// const EncodingPlugin = require('webpack-encoding-plugin')
const { ESBuildMinifyPlugin } = require('esbuild-loader')
const smp = new SpeedMeasurePlugin()

const mode = process.env.NODE_ENV
const isPro = mode === 'production'
console.log('mode:', mode)

module.exports = smp.wrap({
  mode,
  entry: {
    react: './src/react.js',
    // nojs: './src/index.ts',
    // vue: './src/index.js',
  },
  resolve: {
    extensions: ['.jsx', '.tsx', '.ts', '.js'],
  },
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: '[name].[contenthash:8].js',
    clean: true,
  },
  externals: {
    // lodash: {
    //   commonjs: 'lodash',
    //   commonjs2: 'lodash',
    //   amd: 'lodash',
    //   root: '_',
    // },
  },
  module: {
    rules: [
      // {
      //   test: /\.(js|jsx)$/,
      //   loader: 'babel-loader',
      //   exclude: /node_modules/,
      // },
      // {
      //   test: /\.tsx?$/,
      //   use: 'ts-loader',
      //   exclude: /node_modules/,
      // },
      // {
      //   test: /\.(js|tsx?)?$/,
      //   loader: 'esbuild-loader',
      //   options: {
      //     loader: 'tsx',
      //     target: 'es2015',
      //   },
      // },
      {
        test: /\.(js|tsx?)?$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'swc-loader',
        },
      },
      {
        test: /\.(scss|css)$/,
        use: [
          isPro ? MiniCssExtractPlugin.loader : 'style-loader',
          'css-loader',
          'postcss-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        type: 'asset',
      },
      {
        test: /\.(woff(2)?|eot|ttf|otf|svg)$/,
        type: 'asset/resource',
      },
      // {
      //   test: /.txt$/,
      //   use: [
      //     {
      //       loader: path.resolve(__dirname, './loader.js'),
      //     },
      //   ],
      // },
    ],
  },
  plugins: [
    // new EncodingPlugin({
    //   encoding: 'iso-8859-1',
    // }),
    // new CleanWebpackPlugin(),
    // new ESLintPlugin({ extensions: ['js', 'ts', 'tsx'] }),
    new MiniCssExtractPlugin({
      filename: 'css/[name].[contenthash:8].css',
    }),
    // new TinyimgPlugin({
    //   enabled: isPro,
    //   logged: true,
    // }),
    // new CompressImgWebpackPlugin(),
    new HtmlWebpackPlugin({
      title: 'head script',
      template: path.resolve(__dirname, './src/template.html'),
      filename: 'index.html',
      chunks: ['react'],
    }),
    // new webpack.HotModuleReplacementPlugin(),
    // new BundleAnalyzerPlugin(),
    // new webpack.ProvidePlugin({
    //   // _: 'lodash',
    //   keybordJS: 'keyboardjs',
    // }),
    // new WorkboxPlugin.GenerateSW({
    //   // 这些选项帮助快速启用 ServiceWorkers
    //   // 不允许遗留任何“旧的” ServiceWorkers
    //   clientsClaim: true,
    //   skipWaiting: true,
    // }),
    // new ImageMinimizerPlugin({
    //   severityError: 'warning', // Ignore errors on corrupted images
    //   minimizerOptions: {
    //     // plugins: ['gifsicle'],
    //   },
    //   // Disable `loader`
    //   loader: false,
    // }),
  ],
  devServer: {
    historyApiFallback: true,
    contentBase: path.resolve(__dirname, './dist'),
    compress: true,
    host: '0.0.0.0',
    hot: true,
    // port: 8080,
    // https: true,
    proxy: {
      '/api': {
        target: 'https://www.77tianqi.com',
        secure: false,
      },
    },
  },
  devtool: isPro ? false : 'eval-source-map',
  optimization: {
    minimizer: [
      new ESBuildMinifyPlugin({
        target: 'es2015',
      }),
    ],
    // usedExports: false,
    // runtimeChunk: true,
  },
})
