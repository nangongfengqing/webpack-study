// import './styles/main.scss'
import React from 'react'
import ReactDOM from 'react-dom'
// import { Router, Route } from 'react-router'
// import { createBrowserHistory } from 'history'
// import { Provider } from 'mobx-react'
// import Store from './store'
import App from './App'

// const About = () => {
//   return <div>about</div>
// }

ReactDOM.render(<App />, document.getElementById('root'))
