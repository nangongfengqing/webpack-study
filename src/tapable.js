const { SyncHook } = require('tapable')

const queue = new SyncHook(['param1'])

queue.tap('event1', (args) => {
  console.log(args, 1)
})
queue.tap('event2', (args) => {
  console.log(args, 2)
})
queue.tap('event3', (args) => {
  console.log(args, 3)
})

queue.call('try')
