const isObject = (v:object)=> v!==null && typeof v ==='object'

function reactive(obj: object) {
  if (!isObject(obj)) return obj
}

function effect(cb: Function) {}

function track(target: object, key: string) {}

function trigger(target: object, key: string) {}
