// import './styles/app.scss'
// import axios from 'axios'
import React, { useState, useEffect, ReactElement } from 'react'
// import { observer, inject } from 'mobx-react'

// @inject('store')
// @observer
// class App extends Component {
//   // componentDidMount() {}
//   render(): React.ReactElement {
//     return (
//       <>
//         <div className="layout">
//           <aside>
//             <a href="weixin://" className="">
//               唤起
//             </a>
//           </aside>
//           <main></main>
//         </div>
//       </>
//     )
//   }
// }

function useFriendStatus() {
  const [isOnline, setIsOnline] = useState(false)

  useEffect(() => {
    Math.random() > 0.5 ? setIsOnline(true) : setIsOnline(false)
  }, [])

  return isOnline
}

function Richwoman(): ReactElement {
  const [count, setCount] = useState(0)

  useEffect(() => {
    document.title = `clicked ${count} times`
  })

  return (
    <div>
      <div>
        {count}
        {useFriendStatus()}
      </div>
      <button onClick={() => setCount(count + 1)}>林桑正则要加强</button>
    </div>
  )
}

export default Richwoman
