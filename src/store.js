import { observable, action, makeObservable } from 'mobx'

class Store {
  constructor() {
    makeObservable(this)
  }

  @observable
  count = 0

  @action('add')
  add = () => {
    this.count++
  }

  @action('reduce')
  reduce = () => {
    this.count--
  }
}

export default new Store()
